#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <termios.h>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "util.h"

int gps_fd;
char pktbuf[PKTBUF_SIZE];
int pktbuf_head = 0;
int pktbuf_tail = 0;

int
pktbuf_size()
{
	return (PKTBUF_SIZE - pktbuf_head + pktbuf_tail) % PKTBUF_SIZE;
}

int
pktbuf_peek(char *buf, int n)
{
	int i;
	int mypktbuf_head = pktbuf_head;
	for (i = 0; (i < n) && (mypktbuf_head != pktbuf_tail); i++) {
		buf[i] = pktbuf[mypktbuf_head];
		mypktbuf_head++;
		if (mypktbuf_head == PKTBUF_SIZE)
			mypktbuf_head = 0;
	}
	return i;
}

int
pktbuf_deq(char *buf, int n)
{
	int i;
	for (i = 0; (i < n) && (pktbuf_head != pktbuf_tail); i++) {
		buf[i] = pktbuf[pktbuf_head];
		pktbuf_head++;
		if (pktbuf_head == PKTBUF_SIZE)
			pktbuf_head = 0;
	}
	return i;
}

int
pktbuf_enq(char *buf, int n)
{
	int i;

	if (pktbuf_size() + n >= PKTBUF_SIZE)
		return 0;

	for (i = 0; i < n; i++) {
		pktbuf[pktbuf_tail] = buf[i];
		pktbuf_tail++;
		if (pktbuf_tail == PKTBUF_SIZE)
			pktbuf_tail = 0;
	}
	return i;
}

int
gps_send_packet(int fd, Packet_t * pack)
{
	int ret = write(fd, pack, GARMIN_HEADER_SIZE + pack->mDataSize);
	if (ret == -1) {

		perror("oooops problema para enviar pacote");
		exit(1);
	}
	return 0;
}

// magicamente recebe o pacote vindo do garmin
Packet_t *
recvpacket(void)
{
	Packet_t *pkt;
	char tmp[64];
	int nr;

	pkt = (Packet_t *) malloc(GARMIN_MAX_PKTSIZE);
	if (pkt == NULL) {
		perror("malloc failed");
		return NULL;
	}

 chkbuf:
	/* complete packet in buffer? */
	if (pktbuf_size() >= GARMIN_HEADER_SIZE) {
		Packet_t bufpkt;
		pktbuf_peek((char *)&bufpkt, GARMIN_HEADER_SIZE);
		int pktlen = GARMIN_HEADER_SIZE + bufpkt.mDataSize;
		if (pktbuf_size() >= pktlen) {
			pktbuf_deq((char *)pkt, pktlen);
			return pkt;
		}
	}

	/* not enough data - read some */
	nr = read(gps_fd, tmp, 64);
	if (nr == -1) {
		perror("GPS read error");
		free(pkt);
		return NULL;
	}
	if (pktbuf_enq(tmp, nr) == 0)
		fprintf(stderr, "Input buffer full!");

	goto chkbuf;
}

/*
	garmin_pvton()
	turn on position records

	receiver measurement records could also be enabled with 
	command 110 (instead of 49), but we don't need them at present
*/

// envia uma mensagem entrando em modo PVT
void
gps_pvt_on(int fd)
{
	Packet_t *pvtpack = (Packet_t *) malloc(sizeof(Packet_t));

	pvtpack->mPacketType = 20;	// application layer = 20
	pvtpack->mReserved1 = 0;	// reservado
	pvtpack->mReserved2 = 0;	// reservado
	pvtpack->mPacketId = 10;	// pid_command_data = 10
	pvtpack->mReserved3 = 0;	// reservado
	pvtpack->mDataSize = 2;
	pvtpack->mData[0] = 49;		// packet data type, cmnd_start_pvt_data = 49
	pvtpack->mData[1] = 0;

	gps_send_packet(fd, pvtpack);
}

int
connect()
{
	struct termios termios;
	int fd = open("/dev/ttyUSB0", O_RDWR);

	if (fd == -1) {

		perror("ooops: problema sem acessar a porta");
		exit(1);
	}

	tcgetattr(fd, &termios);
	cfmakeraw(&termios);
	tcsetattr(fd, TCIOFLUSH, &termios);

	return fd;
}

void
gps_start(int fd)
{
	u_int32_t privcmd[4];

	privcmd[0] = 0x01106E4B;
	privcmd[1] = 2;
	privcmd[2] = 4;
	privcmd[3] = 0;

	write(fd, privcmd, 16);
}

struct uhet
{
	D800_Pvt_Data_Type _pvt_;
	cpo_sat_data _sd_[12];
};

int
main(int argc, char *argv[])
{
	char nmeabuf[256];

	struct uhet *uhet = create(struct uhet);

	// abre a porta serial para comunicacao
	gps_fd = connect();

	// pacote magico para funcionar 
	gps_start(gps_fd);

	// iniciando comunicacao pvt (position, velocity, and time)
	gps_pvt_on(gps_fd);

	// poling pra receber os dados  
	while (1) {

		Packet_t *pkt;

		// recebendo o primeiro pacote
		pkt = recvpacket();
		// verificando se eh o pacote esperado
		if (pkt->mPacketId == Pid_Pvt_Data) {

			// recuperando o conteudo do pacote 1
			memcpy(&uhet->_pvt_, pkt->mData, sizeof(uhet->_pvt_));
			// recebendo o pacote 2

			free(pkt);
			pkt = NULL;
			pkt = recvpacket();
			// verificando se eh o pacote esperado
			if (pkt->mPacketId == Pid_SatData_Record) {

				// recuperando o conteudo do pacote 2
				memcpy(uhet->_sd_, pkt->mData, sizeof(uhet->_sd_));
			}
		}

		// enquadra os dados recebidos em um pacote
//          memcpy( &_upvt_, pkt->mData, sizeof(_upvt_));
/*			
			if (nmea_gpgga(&lastpvt, NULL, nmeabuf) == 0)	fprintf(stdout, "%s", nmeabuf);
			if (nmea_gprmc(&lastpvt, nmeabuf) == 0)			fprintf(stdout, "%s", nmeabuf);
			if (nmea_gpgll(&lastpvt, nmeabuf) == 0)			fprintf(stdout, "%s", nmeabuf);
			if (nmea_gpgsa(&lastpvt, NULL, nmeabuf) == 0)	fprintf(stdout, "%s", nmeabuf);
*/
		if (nmea_gpgga(&uhet->_pvt_, uhet->_sd_, nmeabuf) == 0)
			fprintf(stdout, "%s", nmeabuf);
		if (nmea_gprmc(&uhet->_pvt_, nmeabuf) == 0)
			fprintf(stdout, "%s", nmeabuf);
		if (nmea_gpgll(&uhet->_pvt_, nmeabuf) == 0)
			fprintf(stdout, "%s", nmeabuf);
		if (nmea_gpgsa(&uhet->_pvt_, uhet->_sd_, nmeabuf) == 0)
			fprintf(stdout, "%s", nmeabuf);
		if (nmea_gpgsv(uhet->_sd_, nmeabuf) == 0)
			fprintf(stdout, "%s", nmeabuf);

		printf("\nnumero de satelites: [%i]",
			   get_number_of_satellites(uhet->_sd_));
		printf("\nhora e data: [%s]", uhet_getutc(&uhet->_pvt_, NULL, NULL));
		printf("\nlatitude: [%s]", uhet_lat(uhet->_pvt_.lat));
		printf("\nlongitude: [%s]", uhet_lon(uhet->_pvt_.lon));
		printf("\nvelocidade: [%s]\n\n", uhet_get_velocidade(&uhet->_pvt_));

		fflush(stdout);

		free(pkt);
	}
	close(gps_fd);
}

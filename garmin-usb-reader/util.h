#ifndef AUX_H
#define AUX_H

#include <sys/types.h>

#pragma pack(push, 1)
typedef struct
{
	u_int8_t mPacketType;
	u_int8_t mReserved1;
	u_int16_t mReserved2;
	u_int16_t mPacketId;
	u_int16_t mReserved3;
	u_int32_t mDataSize;
	u_int8_t mData[1];
} Packet_t;

typedef struct
{
	u_int8_t tag;
	u_int16_t data;
} Protocol_Data_Type;

typedef struct
{
	float alt;
	float epe;
	float eph;
	float epv;
	int16_t fix;
	double tow;
	double lat;
	double lon;
	float east;
	float north;
	float up;
	float msl_hght;
	int16_t leap_scnds;
	int32_t wn_days;
} D800_Pvt_Data_Type;

typedef struct
{
	u_int8_t svid;
	u_int16_t snr;
	u_int8_t elev;
	u_int16_t azmth;
	u_int8_t status;
} cpo_sat_data;

typedef struct
{
	u_int32_t cycles;
	double pr;
	u_int16_t phase;
	u_int8_t slp_dtct;
	u_int8_t snr_dbhz;
	u_int8_t svid;
	u_int8_t valid;
} cpo_rcv_sv_data;

typedef struct
{
	double rcvr_tow;
	u_int16_t rcvr_wn;
	cpo_rcv_sv_data sv[12];
} cpo_rcv_data;
#pragma pack(pop)

enum
{
	Pid_Command_Data = 10,
	Pid_Xfer_Cmplt = 12,
	Pid_Date_Time_Data = 14,
	Pid_Position_Data = 17,
	Pid_Prx_Wpt_Data = 19,
	Pid_Records = 27,
	Pid_Rte_Hdr = 29,
	Pid_Rte_Wpt_Data = 30,
	Pid_Almanac_Data = 31,
	Pid_Trk_Data = 34,
	Pid_Wpt_Data = 35,
	Pid_Pvt_Data = 51,
	Pid_RMR_Data = 52,
	Pid_Rte_Link_Data = 98,
	Pid_Trk_Hdr = 99,
	Pid_SatData_Record = 114,
	Pid_FlightBook_Record = 134,
	Pid_Lap = 149
};
enum
{
	Cmnd_Abort_Transfer = 0, /* abort current transfer */
	Cmnd_Transfer_Alm = 1, /* transfer almanac */
	Cmnd_Transfer_Posn = 2, /* transfer position */
	Cmnd_Transfer_Prx = 3, /* transfer proximity waypoints */
	Cmnd_Transfer_Rte = 4, /* transfer routes */
	Cmnd_Transfer_Time = 5, /* transfer time */
	Cmnd_Transfer_Trk = 6, /* transfer track log */
	Cmnd_Transfer_Wpt = 7, /* transfer waypoints */
	Cmnd_Turn_Off_Pwr = 8, /* turn off power */
	Cmnd_Start_Pvt_Data = 49, /* start transmitting PVT data */
	Cmnd_Stop_Pvt_Data = 50, /* stop transmitting PVT data */
	Cmnd_FlightBook_Transfer = 92, /* start transferring flight records */
	Cmnd_Start_RMR = 110, /* start transmitting Receiver Measurement Records */
	Cmnd_Stop_RMR = 111, /* start transmitting Receiver Measurement Records */
	Cmnd_Transfer_Laps = 117 /* transfer laps */
};

#define NMEA_BUF_SIZE      256
#define NMEA_LATLON_SIZE   16
#define NMEA_UTC_SIZE      16

#define KNOTS_TO_KMH       1.852
#define G_PI               3.14159265358979324
#define rad2deg(x)         ((x) * 180.0 / G_PI)

#define GARMIN_HEADER_SIZE 12
#define GARMIN_MAX_PKTSIZE 512
#define PKTBUF_SIZE        4097

#define create(A) (A*) malloc(sizeof(A))

void nmea_getutc(D800_Pvt_Data_Type * pvt, char *utctime, char *utcdate);
void nmea_fmtlat(double lat, char *latstr);
void nmea_fmtlon(double lon, char *lonstr);
int nmea_gpgga(D800_Pvt_Data_Type * pvt, cpo_sat_data * sat, char *nmeastc);
int nmea_gprmc(D800_Pvt_Data_Type * pvt, char *nmeastc);
int nmea_gpgll(D800_Pvt_Data_Type * pvt, char *nmeastc);
int nmea_gpgsa(D800_Pvt_Data_Type * pvt, cpo_sat_data * sat, char *nmeastc);
int nmea_gpgsv(cpo_sat_data * sat, char *nmeastc);
unsigned char nmea_cksum(char *str);

char *uhet_getutc(D800_Pvt_Data_Type * pvt, char *utctime, char *utcdate);
int get_number_of_satellites(cpo_sat_data * sat);
char *uhet_lat(double lat);
char *uhet_lon(double lon);
char *uhet_get_velocidade(D800_Pvt_Data_Type * pvt);

#endif

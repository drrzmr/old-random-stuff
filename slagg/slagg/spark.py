from pyspark import SparkContext, StorageLevel, SparkConf
from pyspark.sql import SQLContext

from slagg.config import config
from slagg.parser import parser, schema
from slagg import echo


class Filter:

    def __init__(self, fs, params, pattern=config.input.PATTERN):

        self.fs = fs
        self.conf = SparkConf().setAll(params)
        self.parser = parser(pattern)

    def _load(self, sc):
        echo.configs('spark', 'configuration', pairs=sc.getConf().getAll())

        # load files
        rdd = sc.textFile(self.fs.files).map(self.parser)

        df = SQLContext(sc) \
            .createDataFrame(rdd, schema=schema) \
            .persist(StorageLevel.MEMORY_AND_DISK)
        echo.configs('spark', 'loaded dataframe', pairs=(
            ('partitions', df.rdd.getNumPartitions()),
            ('storage.level', str(df.storageLevel)),
        ))
        df.printSchema()
        df.show()

        return df

    def low_throughput(self):
        echo.configs('spark', 'initialize')
        with SparkContext(conf=self.conf) as sc:
            # sort
            sorted_df = self._load(sc) \
                .repartition('userid') \
                .sortWithinPartitions('userid', 'timestamp')
            echo.configs('spark', 'sorted dataframe', pairs=(
                ('partitions', sorted_df.rdd.getNumPartitions()),
                ('storage.level', str(sorted_df.storageLevel)),
            ))
            sorted_df.printSchema()
            sorted_df.show()

            # save
            echo.configs('spark', 'dump dataframe with low throughput')
            sorted_df.write. \
                partitionBy('userid', 'timestamp') \
                .csv(self.fs.spark_path(), quote='')

            return self.fs

    def high_throughput(self):
        echo.configs('spark', 'initialize')
        with SparkContext(conf=self.conf) as sc:
            # sort
            sorted_df = self._load(sc).sort('userid')
            echo.configs('spark', 'loaded dataframe', pairs=(
                ('partitions', sorted_df.rdd.getNumPartitions()),
                ('storage.level', str(sorted_df.storageLevel)),
            ))
            sorted_df.printSchema()
            sorted_df.show()

            # save
            echo.configs('spark', 'dump dataframe with high throughput')
            uid = sorted_df.select('userid').distinct().collect()
            for row_uid in uid:
                with self.fs.output_context(row_uid.userid) as out:

                    by_uid = sorted_df \
                        .where(sorted_df.userid == row_uid.userid) \
                        .sort('timestamp') \
                        .select('line') \
                        .collect()

                    for row in by_uid:
                        out.appendline(row.line)

                yield row_uid.userid

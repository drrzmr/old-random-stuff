from multiprocessing import Pool, current_process
from collections import namedtuple

from slagg.config import config

Worker = namedtuple('Worker', [
    'id',
    'sparkfs',
])

__worker = None


def initializer(sparkfs):
    global __worker

    __worker = Worker(
        id=int(current_process().name.split('-')[1]),
        sparkfs=sparkfs,
    )


def worker(userid):
    global __worker

    return __worker.sparkfs.concat((
        ('userid', userid),
    ))


class Concatenator:
    def __init__(self, sparkfs, workers=config.concatenator.WORKERS):
        self._workers = workers
        self._sparkfs = sparkfs

    @property
    def workers(self):
        return self._workers

    @property
    def sparkfs(self):
        return self._sparkfs

    def do(self):

        def messages():
            for userid in self.sparkfs.values('userid'):
                yield userid

        initargs = (
            self.sparkfs,
        )

        with Pool(self.workers, initializer, initargs=initargs) as pool:
            for filename in pool.imap_unordered(worker, messages()):
                yield filename

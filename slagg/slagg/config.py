from collections import namedtuple
import os

Config = namedtuple('Config', [
    'VERSION',
    'NAME',
    'AUTHOR',
    'AUTHOR_EMAIL',
    'URL',
    'LICENSE',
    'KEYWORDS',
    'DESCRIPTION',
    'fs',
    'input',
    'generator',
    'spark',
    'concatenator'

])

Fs = namedtuple('Fs', [
    'FILE_PATTERN'
])

Input = namedtuple('Input', [
    'PATTERN'
])

Generator = namedtuple('Generator', [
    'WORKERS',
    'USERIDS',
    'FILES',
    'LINES',
    'EXTENSION',
    'CHUNKSIZE'
])

Concatenator = namedtuple('Concatenator', [
    'WORKERS',
    'EXTENSION'
])

__name = 'slagg'

config = Config(
    VERSION='0.2.0',
    NAME=__name,
    AUTHOR='Eder Ruiz',
    AUTHOR_EMAIL='drrzmr@gmail.com',
    URL='https://github.com/drrzmr',
    LICENSE='Apache License 2.0',
    KEYWORDS='spark log processor',
    DESCRIPTION='simple log filter, sort and aggregator',
    fs=Fs(
        FILE_PATTERN='*.log'
    ),
    input=Input(
        PATTERN='^.+ - - \[(.*)\] .* "userid=(.+)"'
    ),
    generator=Generator(
        WORKERS=os.cpu_count(),
        USERIDS=10,
        FILES=10,
        LINES=1000,
        EXTENSION='log',
        CHUNKSIZE=1
    ),
    spark=(
        ('app.name', __name),
        ('master', 'local[{}]'.format(os.cpu_count() // 2)),
        ('rdd.compress', 'True'),
        ('serializer.objectStreamReset', '100'),
        ('submit.deployMode', 'client'),
        ('ui.showConsoleProgress', 'true'),
        ('driver.memory', '4g'),
        ('executor.memory', '4g'),
        ('network.timeout', '600s')
    ),
    concatenator=Concatenator(
        WORKERS=os.cpu_count(),
        EXTENSION='csv',
    )
)

import click
from slagg import config, fs, echo, spark
from slagg.concatenator import Concatenator
from slagg.generator import Generator


@click.group(invoke_without_command=True)
@click.option('--version', '-V', 'version',
              is_flag=True, default=False)
@click.pass_context
def cli(ctx, version):
    if version:
        click.secho('version: ', fg='green', bold=True, nl=False)
        click.echo(config.VERSION)
        return

    if ctx.invoked_subcommand is None:
        print(ctx.get_help())


def output_callback(ctx, param, value):
    del ctx, param

    try:
        return fs.ensure_writable_directory(value)
    except fs.DirectoryException as e:
        raise click.BadParameter(e)


def spark_callback(ctx, param, value):
    del ctx, param

    # load default
    result = {'spark.{}'.format(k): v for k, v in config.spark}

    for param in value:
        try:
            k, v = param.split('=')
        except ValueError as e:
            raise click.BadParameter(
                'Parameter "{}" could not be parsed, expected '
                '"key=value"'.format(param))
        result['spark.{}'.format(k)] = v

    return tuple(result.items())


@cli.group('generate', invoke_without_command=True)
@click.option('--output', '-o', 'outp',
              type=click.Path(file_okay=False), required=True,
              callback=output_callback,
              help='output path, will be created if not exists')
@click.option('--workers', '-w', 'workers',
              type=click.INT, default=config.generator.WORKERS,
              help='number of worker process, each one will simulate one '
                   'server [default: %d]' % config.generator.WORKERS)
@click.option('--userids', '-u', 'userids',
              type=click.INT, default=config.generator.USERIDS,
              help='number of userids to be simulated [default: %d]' %
                   config.generator.USERIDS)
@click.option('--files', '-f', 'files',
              type=click.INT, default=config.generator.FILES,
              help='number of files to be generated [default: %d]' %
                   config.generator.FILES)
@click.option('--lines', '-l', 'lines',
              type=click.INT, default=config.generator.LINES,
              help='number of lines for each generated file [default: %d]' %
                   config.generator.LINES)
@click.option('--extension', '-e', 'extension',
              type=click.STRING, default=config.generator.EXTENSION,
              help='files extension [default: %s]' % config.generator.EXTENSION)
@click.option('--chunksize', '-c', 'chunksize',
              type=click.INT, default=config.generator.CHUNKSIZE,
              help='size of chunks to be used with workers pool [default: '
                   '%d]' % config.generator.CHUNKSIZE)
@click.pass_context
def cli_generate(ctx, outp, workers, userids, files, lines, extension,
                 chunksize):
    if ctx.invoked_subcommand is not None:
        print(ctx.get_help())
        return

    echo.configs(pairs=(
        ('output', outp),
        ('workers', workers),
        ('userids', userids),
        ('files', files),
        ('lines', lines),
        ('extension', extension),
        ('chunksize', chunksize)
    ))

    gen = Generator(path=outp, n_workers=workers, n_userids=userids,
                    n_files=files, n_lines=lines, extension=extension,
                    chunksize=chunksize)
    for fn in gen.run():
        print('->', fn)


@cli.group('filter')
def cli_filter():
    pass


@cli_filter.command('lt', help='low throughput filter, but secure. All data '
                               'are managed by spark, not data come from '
                               'spark to python memory, so no out of memory '
                               'exception are possible')
@click.option('--input', '-i', 'inp',
              type=click.Path(exists=True, file_okay=False, resolve_path=True),
              required=True, multiple=True,
              help='input path, could be used multiple times for multiple '
                   'paths')
@click.option('--output', '-o', 'outp',
              type=click.Path(file_okay=False), required=True,
              help='output path, will be created')
@click.option('--pattern', '-p', 'pattern',
              type=click.STRING, default=config.fs.FILE_PATTERN,
              help='pattern to files match [default: %s]' %
                   config.fs.FILE_PATTERN)
@click.option('--recursive', '-r', 'recursive',
              is_flag=True, default=False,
              help='find files recursively')
@click.option('--workers', '-w', 'workers',
              type=click.INT, default=config.concatenator.WORKERS,
              help='number of worker process, each one will concat one userid ['
                   'default: %d]' % config.concatenator.WORKERS)
@click.option('--spark', '-s', 'spark_params',
              multiple=True, callback=spark_callback,
              help='values to be passed to SparkConf [ex: '
                   'executor.memory=8g], multiple are permitted')
def cli_filter_lt(inp, outp, pattern, recursive, workers, spark_params):
    files = fs.list_files(inp, pattern, recursive)
    echo.configs(pairs=(
        ('spark params', spark_params),
        ('workers', workers),
        ('output', outp),
        ('pattern', pattern),
        ('recursive', recursive),
        ('files', files)
    ))

    try:
        sparkfs = fs.SparkFs(outp, files)
    except ValueError as e:
        raise click.BadParameter(e)

    sparkfilter = spark.Filter(sparkfs, spark_params)

    try:
        filtered_fs = sparkfilter.low_throughput()
    except Exception as e:
        click.secho("something is wrong with filter, sorry :(", fg='red',
                    bold=True)
        raise e

    concat = Concatenator(filtered_fs, workers=workers)
    for filename in concat.do():
        echo.config('generated file', filename)


@cli_filter.command('ht', help='high throughput filter, but less secure. '
                               'Filter will iterate over userids, and take '
                               'data for all timestamps for each userid, '
                               'and write only one file. if there are more '
                               'data than memory, a out od memory exception'
                               'will be thrown')
@click.option('--input', '-i', 'inp',
              type=click.Path(exists=True, file_okay=False, resolve_path=True),
              required=True, multiple=True,
              help='input path, could be used multiple times for multiple '
                   'paths')
@click.option('--output', '-o', 'outp',
              type=click.Path(file_okay=False), required=True,
              help='output path, will be created')
@click.option('--pattern', '-p', 'pattern',
              type=click.STRING, default=config.fs.FILE_PATTERN,
              help='pattern to files match [default: %s]' %
                   config.fs.FILE_PATTERN)
@click.option('--recursive', '-r', 'recursive',
              is_flag=True, default=False,
              help='find files recursively')
@click.option('--spark', '-s', 'spark_params',
              multiple=True, callback=spark_callback,
              help='values to be passed to SparkConf [ex: '
                   'executor.memory=8g], multiple are permitted')
def cli_filter_ht(inp, outp, pattern, recursive, spark_params):
    files = fs.list_files(inp, pattern, recursive)
    echo.configs(pairs=(
        ('spark params', spark_params),
        ('output', outp),
        ('pattern', pattern),
        ('recursive', recursive),
        ('files', files)
    ))

    try:
        sparkfs = fs.SparkFs(outp, files)
    except ValueError as e:
        raise click.BadParameter(e)

    sparkfilter = spark.Filter(sparkfs, spark_params)

    try:
        filtered_fs = sparkfilter.high_throughput()
    except Exception as e:
        click.secho("something is wrong with filter, sorry :(", fg='red',
                    bold=True)
        raise e

    for filename in filtered_fs:
        echo.config('generated file', filename)


def main():
    echo.title(config.NAME, config.DESCRIPTION)

    cli()


if __name__ == '__main__':
    main()

from collections import namedtuple
from multiprocessing import Pool, current_process
import random
import uuid
from datetime import datetime, timezone, timedelta

from slagg import fs
from slagg.config import config

LogData = namedtuple('LogData', [
    'method',
    'status_code',
    'request_size_range',
    'resource',
    'ipaddr',
    'date_timezone',
    'date_fmt',
    'userid'
])

LogFile = namedtuple('LogFile', [
    'lines',
    'path',
    'extension'
])

_FakeLog = namedtuple('_FakeLog', [
    'data',
    'file',
    'worker_id',
])


class FakeLog(_FakeLog):

    @property
    def line(self):
        params = {
            'ipaddr': random.choice(self.data.ipaddr),
            'method': random.choice(self.data.method),
            'status_code': random.choice(self.data.status_code),
            'request_size': random.randint(*self.data.request_size_range),
            'resource': random.choice(self.data.resource),
            'userid': random.choice(self.data.userid),
            'timestamp': datetime(
                year=random.randint(2016, 2017),
                month=random.randint(1, 12),
                day=random.randint(1, 28),
                hour=random.randint(0, 23),
                minute=random.randint(0, 59),
                second=random.randint(0, 59),
                tzinfo=timezone(timedelta(hours=-3))  # brasilia tz
            ).strftime(self.data.date_fmt)
        }

        return '{ipaddr} - - [{timestamp}] "{method} {resource} HTTP/1.1" ' \
               '{status_code} {request_size} "-" "userid={userid}"\n'. \
            format(**params)

    @property
    def filename(self):
        return fs.join(self.file.path, '%03d.%s' % (count, self.file.extension))


fake_log = None
count = 1


def initializer(path, userids, n_lines, extension):
    global fake_log

    worker_id = int(current_process().name.split('-')[1])
    path = fs.ensure_writable_directory(fs.join(path, '%03d' % worker_id))

    random.seed(worker_id)

    fake_log = FakeLog(
        worker_id=worker_id,
        data=LogData(
            method=('GET', 'POST', 'PUT', 'DELETE'),
            status_code=(200, 201, 400, 500),
            request_size_range=(1, 1000),
            ipaddr=(['192.168.0.%i' % i for i in range(1, 255)]),
            resource=('/a.html', '/e.html', '/i.html', '/o.html', '/u.html'),
            userid=userids,
            date_fmt='%d/%b/%Y:%H:%M:%S %z',
            date_timezone=timezone(timedelta(hours=-3))
        ),
        file=LogFile(
            path=path,
            extension=extension,
            lines=n_lines
        )
    )


def func(_):
    global count

    fn = fake_log.filename
    with open(fn, 'w') as file:
        for line in range(fake_log.file.lines):
            file.write(fake_log.line)

        count += 1
    return fn


class Generator:

    def __init__(self, path,
                 n_workers=config.generator.WORKERS,
                 n_userids=config.generator.USERIDS,
                 n_files=config.generator.FILES,
                 n_lines=config.generator.LINES,
                 extension=config.generator.EXTENSION,
                 chunksize=config.generator.CHUNKSIZE):
        self._path = path
        self._n_workers = n_workers
        self._userids = ([str(uuid.uuid4()) for _ in range(n_userids)])
        self._n_files = n_files
        self._n_lines = n_lines
        self._extension = extension
        self._chunksize = chunksize

    def _messages(self):
        return range(1, self._n_files * self._n_workers + 1)

    def _initargs(self):
        return (
            self._path,
            self._userids,
            self._n_lines,
            self._extension
        )

    def run(self):
        with Pool(self._n_workers, initializer, self._initargs()) as pool:
            for fn in pool.imap_unordered(func,
                                          self._messages(),
                                          self._chunksize):
                yield fn

import os
import glob
import fnmatch
import shutil


class InvalidKeyValueDirectoryException(Exception):

    def __init__(self, *args, **kwargs):
        super(InvalidKeyValueDirectoryException, self).__init__(args, kwargs)


class DirectoryException(Exception):

    def __init__(self, *args, **kwargs):
        super(DirectoryException, self).__init__(args, kwargs)


def join(path, fn):
    return os.path.abspath(os.path.join(path, fn))


def ensure_writable_directory(path):
    path = os.path.abspath(path)

    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except PermissionError:
            raise DirectoryException(
                'Directory "%s" could not be created' % path)
        # could be created by other process, thread or external
        except FileExistsError:
            pass
    elif not os.path.isdir(path):
        raise DirectoryException('Path "%s" is not directory'
                                 % path)

    if not os.access(path, os.W_OK):
        raise DirectoryException('Directory "%s" are not '
                                 'writeable' % path)

    return path


def ilist_files(paths, pattern, recursive=False):
    paths = [paths] if isinstance(paths, str) else paths

    if not recursive:
        for path in paths:
            path_with_pattern = os.path.join(os.path.expanduser(path), pattern)
            for filepath in glob.iglob(path_with_pattern):
                yield filepath
        return

    for path in paths:
        for root, dirs, files in os.walk(path):
            for file in fnmatch.filter(files, pattern):
                yield os.path.join(root, file)


def list_files(paths, pattern, recursive=False):
    return tuple(
        {filepath for filepath in ilist_files(paths, pattern, recursive)}
    )


class SparkFs:

    def __init__(self, path, files, spark_ns='spark'):
        if not files:
            raise ValueError('Empty files list')

        if os.path.exists(path):
            raise ValueError('Path already exists', path)

        self._path = ensure_writable_directory(path)
        self._files = files if isinstance(files, str) else ','.join(files)
        self._spark_dir = os.path.join(self._path, spark_ns)

    @property
    def files(self):
        return self._files

    def path(self, branch=None):
        branch = [] if branch is None else branch
        branch = ['{}={}'.format(k, v) for k, v in branch]
        return os.path.join(self._path, *branch)

    def spark_path(self, branch=None):
        branch = [] if branch is None else branch
        branch = ['{}={}'.format(k, v) for k, v in branch]
        return os.path.join(self._spark_dir, *branch)

    def listall(self, branch=None):
        root = self.spark_path(branch)

        ret = []
        for item in os.listdir(root):
            ret.append(item)

        for item in sorted(ret):
            yield item

    def listdir(self, branch=None, join=False):
        for item in self.listall(branch):
            p = os.path.join(self.spark_path(branch), item)

            if not os.path.isdir(p):
                continue

            if not join:
                yield item
                continue

            yield p

    def values(self, key, branch=None):
        for item in self.listdir(branch):
            path = os.path.join(self.spark_path(branch), item)

            try:
                k, v = os.path.basename(path).split('=')
            except ValueError as e:
                raise InvalidKeyValueDirectoryException(
                    'Not valid directory', path)
            if k == key:
                yield v

    def concat(self, branch):
        dstfn = os.path.join(self._path, branch[-1][1])  # last value
        with open(dstfn, 'w') as dst:
            for srcdn in self.listdir(branch, join=True):
                for fn in ilist_files(srcdn, '*.csv'):
                    with open(fn) as src:
                        shutil.copyfileobj(src, dst)

        return dstfn

    def output_context(self, resource_name, branch=None):
        filename = os.path.join(self.path(branch), resource_name)
        writer = _Writer(self, filename)
        return Output(writer)


class _Writer:

    def __init__(self, fs, filename):
        self._fs = fs
        self._fn = filename
        self._opened_file = None

    def open(self):
        self._opened_file = open(self._fn, 'w')

    def close(self):
        self._opened_file.close()

    def appendline(self, line):
        self._opened_file.write('{}\n'.format(line))


class Output:
    def __init__(self, writer):
        self._writer = writer

    def __enter__(self):
        self._writer.open()
        return self._writer

    def __exit__(self, *args):
        self._writer.close()

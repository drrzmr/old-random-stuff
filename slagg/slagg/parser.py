from pyspark.sql import Row
from pyspark.sql.types import *
import re
from datetime import datetime, timezone

from slagg.config import config


class ParserException(Exception):

    def __init__(self, *args, **kwargs):
        super(ParserException, self).__init__(*args, **kwargs)


schema = StructType([
    StructField('userid', StringType(), False),
    StructField('timestamp', LongType(), False),
    StructField('line', StringType(), False)
])


def parser(pattern=config.input.PATTERN):
    regex = re.compile(pattern)

    def inner_parser(line):
        match = regex.search(line)
        try:
            line = match.group(0)
            ts = match.group(1)
            userid = match.group(2)
        except AttributeError:
            raise ParserException('Could not parse line "{}"'.format(line))

        dt = datetime \
            .strptime(ts, '%d/%b/%Y:%H:%M:%S %z') \
            .astimezone(timezone.utc)  # everybody goes to utc

        timestamp = int(dt.strftime('%Y%m%d%H%M%S'))

        return Row(userid=userid, timestamp=timestamp, line=line)

    return inner_parser

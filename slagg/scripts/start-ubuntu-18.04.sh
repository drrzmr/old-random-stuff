#!/bin/bash

set -e

export LC_ALL=C.UTF-8
export LANG=C.UTF-8

source slagg-env/bin/activate
echo
echo
echo " => generate fake logs"
echo
echo
slagg generate -o generated-logs
echo
echo
echo " => filter generated logs"
echo
echo
slagg filter ht -r -i generated-logs -o filtered-logs
echo
echo

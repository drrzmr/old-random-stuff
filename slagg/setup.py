from setuptools import setup
from slagg import config

setup(
    author=config.AUTHOR,
    author_email=config.AUTHOR_EMAIL,
    license=config.LICENSE,
    description=config.DESCRIPTION,
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    keywords=config.KEYWORDS,
    url=config.URL,
    version=config.VERSION,
    zip_safe=False,
    install_requires=open('requirements.txt').read().splitlines(),
    name=config.NAME,
    packages=[config.NAME],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            '%s = %s.main:main' % (config.NAME, config.NAME),
        ]
    },
    classifiers=(
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: %s' % config.LICENSE,
        'Operating System :: OS Independent',
    )
)

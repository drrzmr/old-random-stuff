import unittest
import os
import shutil

from slagg import fs


class TestSparkFs(unittest.TestCase):

    def setUp(self):
        self.fs = fs.SparkFs('test_spark_fs', ['a', 'b'],
                             spark_ns='test_spark')
        os.makedirs(os.path.join(self.fs.spark_path(), 'userid=1'))
        os.mkdir(os.path.join(self.fs.spark_path(), 'userid=2'))

    def tearDown(self):
        shutil.rmtree('test_spark_fs')

    def test_spark_dir(self):
        x = self.fs.spark_path().split(os.sep)[-2:]
        spark_dir = os.path.join(*x)
        self.assertEqual(spark_dir, os.path.join('test_spark_fs', 'test_spark'))

    def test_listall(self):
        result = [i for i in self.fs.listall()]
        self.assertEqual(result[0], 'userid=1')
        self.assertEqual(result[1], 'userid=2')

    def test_listdir(self):
        result = [i for i in self.fs.listdir()]
        self.assertEqual(result[0], 'userid=1')
        self.assertEqual(result[1], 'userid=2')

        result = [i.split(os.sep)[-1] for i in self.fs.listdir(join=True)]
        self.assertEqual(result[0], 'userid=1')
        self.assertEqual(result[1], 'userid=2')

    def test_values(self):
        values = [v for v in self.fs.values('userid')]
        self.assertEqual(values[0], '1')
        self.assertEqual(values[1], '2')


if __name__ == '__main__':
    unittest.main()

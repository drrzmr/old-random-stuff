#ifndef __GLUDA_H__
#define __GLUDA_H__

#include "common.h"

#include <stdbool.h>

BEGIN_DECLS

typedef void (*keyboard_cb_t)(unsigned char key, int x, int y);
typedef const unsigned char * (*run_cb_t)(void *user_data);
typedef void (*exit_cb_t)(void *user_data);

typedef struct
{
	void *user_data;
	keyboard_cb_t keyboard_cb;
	run_cb_t run_cb;
	exit_cb_t exit_cb;
} gluda_t;

bool
gluda_init(gluda_t *g, unsigned rows, unsigned cols);

bool
gluda_run(int *argcp, char *argv[]);

END_DECLS

#endif /* __GLUDA_H__ */

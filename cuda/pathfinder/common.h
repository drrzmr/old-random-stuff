#ifndef __COMMON_H__
#define __COMMON_H__

#ifdef  __cplusplus
#define BEGIN_DECLS  extern "C" {
#define END_DECLS    }
#else
#define BEGIN_DECLS
#define END_DECLS
#endif

#if defined(__STDC__)
# if defined(__STDC_VERSION__)
#  if (__STDC_VERSION__ == 199901L)
#   define C99
#  endif
# endif
#endif

#include <stdio.h>

#if defined(__GNUC__)
# define log(fmt, args...) \
   fprintf(stderr, "%s:%d [%s] -> " fmt "\n", __FILE__, __LINE__, __FUNCTION__, ##args);
#elif defined(C99)
# define log(fmt, __VA_ARGS__) \
   fprintf(stderr, "%s:%d [%s] -> " fmt "\n", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);
#else
# define log
#endif /* C99 */

#endif /* __COMMON_H__ */

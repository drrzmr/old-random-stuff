#include "gluda.h"

#include <stdlib.h>

#include <GL/glut.h>

#include "common.h"

#define KEY_ENTER 13
#define KEY_ESC 27

static bool _init = false;
static void * _user_data = NULL;

static keyboard_cb_t _keyboard_cb = NULL;
static run_cb_t _run_cb = NULL;
static exit_cb_t _exit_cb = NULL;

static const unsigned char *_pixels = NULL;
static unsigned _rows;
static unsigned _cols;
static unsigned char _k_key;
static int _k_x;
static int _k_y;
static int _m_button;
static int _m_state;
static int _m_x;
static int _m_y;

static void
keyboard(unsigned char key, int x, int y)
{
	_k_key = key;
	_k_x = x;
	_k_y = y;

	log("key: %d, x: %d, y: %d", _k_key, _k_x, _k_y);

	if (NULL != _keyboard_cb) {
		_keyboard_cb(key, x, y);
		return;
	}
}

static void
mouse(int button, int state, int x, int y)
{
	log("button: %d, state: %d, x: %d, y: %d", _m_button, _m_state, _m_x, _m_y);

	_m_button = button;
	_m_state = state;
	_m_x = x;
	_m_y = y;
}

static void
idle(void)
{
	switch (_k_key) {
	case KEY_ENTER:
		/*
		 * chamar a funcao do usuario que realiza a animacao na gpu e retorna o
		 * buffer a ser pintado
		 */

		if (NULL == _run_cb) {
			glutPostRedisplay();
			log(".");
			break;
		}

		/* update pixel buffer com resultado da animacao */
		_pixels = _run_cb(_user_data);
		glutPostRedisplay();
		break;

	case KEY_ESC:
		if (NULL != _exit_cb) {
			_exit_cb(_user_data);
		}

		exit(0);
		break;
	}
	_k_key = 0;

}

static void
display(void)
{
	if (NULL == _pixels) {
		log(".");
		return;
	}

	glPixelZoom(1,-1);
	glRasterPos2f(-1,1.0);
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glDrawPixels(_cols, _rows, GL_RGBA, GL_UNSIGNED_BYTE, _pixels);
	glutSwapBuffers();
}

static void
reshape(int width, int height)
{
	log("width: %d, height: %d", width, height);
	_pixels = _run_cb(_user_data);
	glutPostRedisplay();
}

bool
gluda_init(gluda_t *g, unsigned rows, unsigned cols)
{
	if (true == _init) {
		log("gluda just initialized!");
		return false;
	}

	_user_data = g->user_data;
	_rows = rows;
	_cols = cols;

	_keyboard_cb = g->keyboard_cb;
	_run_cb = g->run_cb;
	_exit_cb = g->exit_cb;

	_init = true;

	return true;
}

bool
gluda_run(int *argcp, char *argv[])
{
	if (false == _init) {
		log("gluda must be initialized");
		return false;
	}

	glutInit(argcp, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(_cols, _rows);
	glutCreateWindow("gluda");
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutMouseFunc(mouse);
	glutIdleFunc(idle);
	glutMainLoop();

	return true;
}

#ifndef __PARSER_H__
#define __PARSER_H__

#include "common.h"
#include "matrix.h"

BEGIN_DECLS

matrix_t* parser_run(const char *filename);

END_DECLS

#endif /* __PARSER_H__ */

#ifndef __CUDA_COMMON_H__
#define __CUDA_COMMON_H__

#if defined(__NVCC__)

#include <stdlib.h>
#include <stdio.h>

#define CHECK(call) \
do { \
	if (cudaSuccess != call) { \
		fprintf(stderr, ("CUDA ERROR! file: %s[%i] -> %s\n"), __FILE__, __LINE__, cudaGetErrorString(call)); \
		exit(0); \
	} \
} while (0)

#endif /* __NVCC__ */

#endif /* __CUDA_COMMON_H__ */

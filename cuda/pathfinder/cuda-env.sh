#!/bin/bash

test -z $CUDA_ROOT && CUDA_ROOT="/opt/cuda-6.5"
export CUDA_ROOT

INIT_FILES=""
INIT_FILES="${INIT_FILES}/etc/profile "
INIT_FILES="${INIT_FILES}${HOME}/.bashrc"
echo "init files: ${INIT_FILES}"
echo "cuda root: ${CUDA_ROOT}"

bash --init-file <(
	cat ${INIT_FILES};
	echo 'PS1="[cuda] $PS1"';
	echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CUDA_ROOT/lib64';
	echo 'export PATH=$PATH:$CUDA_ROOT/bin'
) -i

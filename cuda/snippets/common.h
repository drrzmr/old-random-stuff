#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>
#include <cuda_runtime.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CHECK_ERROR(call) \
do { \
	if (cudaSuccess != call) { \
		fprintf(stderr, ("CUDA ERROR! file: %s[%i] -> %s\n"), __FILE__, __LINE__, cudaGetErrorString(call)); \
		exit(0); \
	} \
} while (0)

#define GETPOS(row,col,width) \
({ \
	(col + row * width); \
})

void print_array(const int *array, const unsigned len);
void print_matriz(const int *m, unsigned width, unsigned height);

#ifdef __cplusplus
}
#endif

#endif // __COMMON_H__

#ifndef __SENSOR_H__
#define __SENSOR_H__

#include "list.h"

#define ARRAY_SENSOR_MAX 100

struct sensor
{
	struct list *sensor;
	pthread_mutex_t mutex;
};

struct sensor *sensor_clear(struct sensor *sensor, int tam);

struct sensor *sensor_parser(struct sensor *sensor_array, struct list *list);

struct sensor *sensor_create(int tam);

#endif // __SENSOR_H__

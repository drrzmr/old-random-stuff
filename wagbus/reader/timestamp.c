#include "timestamp.h"

uint64_t
getTimeMiliseconds()
{
	uint64_t tm = 0;
	struct timeval t;
	gettimeofday(&t, NULL);
	tm = t.tv_sec;
	tm *= 1000;
	tm += (t.tv_usec / 1000);
	return tm;
}

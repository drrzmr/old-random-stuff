#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <signal.h>

#include "list.h"
#include "buffer.h"
#include "package.h"
#include "wagbus.h"
#include "sensor.h"

#define TRUE 1
#define FALSE 0

void
handler_ctrl_c(int signal)
{
	printf("sinal: %i\n", signal);
}

struct wagbus *wagbus = NULL;
int crc_erro;

void *
start(void *arg)
{
	struct wagbus *w = (struct wagbus *)arg;
	while (1) {
		w = wagbus_watch_port(w);
		w = wagbus_parser(w);
		wagbus_list_print(w);
		//atualiza listas de amostras de cada sensor
		//w->sensor_array = sensor_parser(w->sensor_array, w->list);
		//printf("tamanho do buffer  %d\n", w->buffer->lenght);
		//limpa lista de amostras
		//wagbus_flush_list(w);
	}
	return NULL;
}

int
main()
{
	pthread_t th;
	signal(SIGINT, handler_ctrl_c);
	wagbus = wagbus_connect("/dev/ttyUSB0", B9600, 200, 1000 * 500);
	int ret = pthread_create(&th, NULL, start, (void *)wagbus);

	while (1) {
		if (wagbus->buffer)
			printf("tamanho do buffer: %i | ", wagbus->buffer->lenght);

		printf
			("pacotes: %li, fragmentados: %li, bytes invalidos: %li, erro crc: %i\n",
			 wagbus->pkg_count, wagbus->pkg_fragment, wagbus->pkg_invalid_byte,
			 crc_erro);

		if (wagbus->list)
			printf("tamanho da lista: %i\n", wagbus->list->lenght);

		int i;
		for (i = 0; i < ARRAY_SENSOR_MAX; i++) {

			struct sensor *sensor = &(wagbus->sensor_array[i]);

			pthread_mutex_lock(&sensor->mutex);
			if (sensor->sensor) {
				if (sensor->sensor->lenght > 0) {
					printf("sensor: %.2X, numero de pacotes: %i\n", i,
						   sensor->sensor->lenght);
				}
			}
			pthread_mutex_unlock(&sensor->mutex);
		}

		wagbus->sensor_array =
			sensor_clear(wagbus->sensor_array, ARRAY_SENSOR_MAX);

		sleep(5);
	}
	return ret;
}

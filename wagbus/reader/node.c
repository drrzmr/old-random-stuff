#include <stdlib.h>

#include "node.h"

#define create(x) (x*) malloc(sizeof(x))

struct node *
node_init(void *data)
{
	struct node *node = create(struct node);
	node->next = NULL;
	node->data = data;
	return node;
}

#include <stdlib.h>
#include <pthread.h>

#include "package.h"
//#include "list.h"

#include "sensor.h"

struct sensor *
sensor_create(int tam)
{
	struct sensor *sensor_array;
	int i;

	sensor_array = (struct sensor *)malloc(sizeof(struct sensor) * tam);

	for (i = 0; i < tam; i++) {

		pthread_mutex_init(&(sensor_array[i].mutex), NULL);
		//sensor_array[i].sensor = list_init();
		sensor_array[i].sensor = NULL;
	}
	return sensor_array;
}

struct sensor *
sensor_clear(struct sensor sensor[], int tam)
{
	struct sensor *s = sensor;
	int i;

	for (i = 0; i < tam; i++) {

		pthread_mutex_lock(&(s->mutex));

		s->sensor = list_clear(s->sensor);

		pthread_mutex_unlock(&(s->mutex));

		s++;
	}
	return sensor;
}

struct sensor *
sensor_parser(struct sensor *sensor_array, struct list *list)
{
	struct node *it = NULL;
	struct package *p = NULL;
	struct package *p_aux = NULL;
	//struct list* lista = list_init();
	struct list *lista = NULL;
	struct sensor *sensor;
	int id;

	if (list) {

		list->reset_it(list);
		while ((it = list->get_it(list))) {

			p = (struct package *)list->get_data_from_it(it);
			p_aux = package_clone(p);

			id = p->header->sensor_id;
			sensor = &(sensor_array[id]);

			pthread_mutex_lock(&(sensor->mutex));

			lista = (sensor->sensor == NULL) ? list_init() : sensor->sensor;
			lista->push_back(lista, (void *)p_aux);
			sensor->sensor = lista;

			pthread_mutex_unlock(&(sensor->mutex));
		}
	}
	return sensor_array;
}

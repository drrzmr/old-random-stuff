#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "package.h"

extern int crc_erro;

// funcao private
int calc_crc(int oldcrc, int newbyte);

struct package *
package_create()
{
	struct package *p = create(struct package);
	p->header = create(struct header);
	p->payload = create(struct payload);

	memset(p->header, 0, sizeof(struct header));
	p->payload->data = NULL;
	p->payload->crc = 0;

	return p;
}

struct package *
package_delete(struct package *p)
{
	if (p) {
		if (p->header) {

			free(p->header);
			p->header = NULL;
		}
		if (p->payload) {

			if (p->payload->data) {
				free(p->payload->data);
				p->payload->data = NULL;
			}
			free(p->payload);
			p->payload = NULL;
		}
		free(p);
		p = NULL;
	}
	return NULL;
}

struct package *
package_print(struct package *package)
{
	if (package) {
		if (package->header) {
			printf("[%.2X ", package->header->start);
			printf("%.2X ", package->header->package_seq);
			printf("%.2X ", package->header->sensor_id);
			printf("%.2X ", package->header->data_type);
			printf("%.2X] ", package->header->data_lenght);

			int i;
			for (i = 0; i < package->header->data_lenght; i++) {

				printf("%s%.2X%s ",
					   VERMELHO, package->payload->data[i], NORMAL);
			}

			printf("%.2X | ", package->payload->crc);

			int crc = package_calc_crc(package);

			if (crc == package->payload->crc) {
				printf("%s[%.2X]%s", NORMAL, crc, NORMAL);
			} else {
				crc_erro++;
				printf("%s[%.2X]%s", VERDE, crc, NORMAL);
			}

			//printf("\n");
			return package;
		} else {
			fprintf(stderr, "header NULL");
			exit(-1);
		}
	} else {
		fprintf(stderr, "package NULL");
		exit(-1);
	}
	return NULL;
}

// funcao privada
int
calc_crc(int oldcrc, int newbyte)
{
	int shift_reg, data_bit, sr_lsb, fb_bit, j;
	shift_reg = oldcrc;

	for (j = 0; j < 8; j++) {
		data_bit = (newbyte >> j) & 0x01;
		sr_lsb = shift_reg & 0x01;
		fb_bit = (data_bit ^ sr_lsb) & 0x01;
		shift_reg = shift_reg >> 1;

		if (fb_bit)
			shift_reg = shift_reg ^ 0x8c;
	}
	return (shift_reg);
}

uint8_t
package_calc_crc(struct package * p)
{
	uint8_t crc = 0;
	crc = calc_crc(crc, p->header->start);
	crc = calc_crc(crc, p->header->package_seq);
	crc = calc_crc(crc, p->header->sensor_id);
	crc = calc_crc(crc, p->header->data_type);
	crc = calc_crc(crc, p->header->data_lenght);

	int i;
	for (i = 0; i < p->header->data_lenght; i++) {
		crc = calc_crc(crc, p->payload->data[i]);
	}
	return crc;
}

struct package *
package_make(struct package *p, uint8_t * from)
{
	if (from) {

		// struct package* p = package_create();
		memcpy(p->header, from, sizeof(struct header));
		p->payload->data = (uint8_t *) malloc(p->header->data_lenght);
		from += sizeof(struct header);
		memcpy(p->payload->data, from, p->header->data_lenght);
		from += p->header->data_lenght;
		p->payload->crc = *from;
		return p;
	}
	return NULL;
}

struct package *
package_clone(struct package *p)
{
	struct package *pkg_new = package_create();
	//header
	pkg_new->header->start = p->header->start;
	pkg_new->header->package_seq = p->header->package_seq;
	pkg_new->header->sensor_id = p->header->sensor_id;
	pkg_new->header->data_type = p->header->data_type;
	pkg_new->header->data_lenght = p->header->data_lenght;
	//memcpy(pkg_new->payload,p->payload,sizeof(struct payload));

	//payload
	pkg_new->payload->crc = p->payload->crc;
	pkg_new->payload->data = (uint8_t *) malloc(pkg_new->header->data_lenght);
	memcpy(pkg_new->payload->data, p->payload->data, p->header->data_lenght);

#if DEBUG >= 10
	printf("pacote original\n");
	package_print(p);
	printf("\n");
	printf("pacote clone\n");
	package_print(pkg_new);
	printf("\n");
#endif

	return pkg_new;
}

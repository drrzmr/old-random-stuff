#ifndef __BUFFER_H__
#define __BUFFER_H__
#include <stdint.h>

#define create(x) (x*) malloc(sizeof(x))

struct buffer
{
	uint8_t *data;
	unsigned int lenght;
	struct buffer *(*load) ();
	void (*parser) ();
	struct buffer *(*delete) ();

};

struct buffer *buffer_create(unsigned int lenght);

struct buffer *buffer_load(struct buffer *buffer, int fd);

struct buffer *buffer_delete(struct buffer *buffer);

struct buffer *buffer_flush(struct buffer *buffer);

void buffer_parser(struct buffer *buffer);

struct buffer *buffer_concat(struct buffer *b1, struct buffer *b2);

struct buffer *buffer_trunc(struct buffer *b, int index);

void buffer_print(uint8_t * b, int ini, int fim);

#endif // __BUFFER_H__

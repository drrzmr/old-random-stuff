#include <stdio.h>
#include <stdint.h>

int
calc_crc(int oldcrc, int newbyte)
{
	int shift_reg, data_bit, sr_lsb, fb_bit, j;
	shift_reg = oldcrc;

	for (j = 0; j < 8; j++) {

		data_bit = (newbyte >> j) & 0x01;
		sr_lsb = shift_reg & 0x01;
		fb_bit = (data_bit ^ sr_lsb) & 0x01;
		shift_reg = shift_reg >> 1;

		if (fb_bit)
			shift_reg = shift_reg ^ 0x8c;
	}
	return shift_reg;
}

int
main(void)
{
	uint8_t crc = 0;

	crc = calc_crc(crc, 0x0A);
	crc = calc_crc(crc, 0x45);
	crc = calc_crc(crc, 0x10);
	crc = calc_crc(crc, 0x02);
	crc = calc_crc(crc, 0x03);
	crc = calc_crc(crc, 0x0B);
	crc = calc_crc(crc, 0x16);
	crc = calc_crc(crc, 0x21);

	printf("crc: %.02X\n", crc);
}

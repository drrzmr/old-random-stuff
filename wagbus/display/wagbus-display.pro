#-------------------------------------------------
#
# Project created by QtCreator 2009-05-13T23:17:26
#
#-------------------------------------------------

TARGET = wagbus-display
TEMPLATE = app


SOURCES += main.cpp \
    window.cpp

HEADERS  += \
    window.h

FORMS    += \
    window.ui

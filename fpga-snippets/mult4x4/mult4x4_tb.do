vlib work
vlog mult4x4.v mult4x4_tb.v
vsim -t ns work.mult4x4_tb
view wave
add wave -radix unsigned /a
add wave -radix unsigned /b
add wave -radix unsigned /p
run 10 ns
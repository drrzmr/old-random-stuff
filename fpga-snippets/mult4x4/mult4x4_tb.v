`timescale 1 ns/1 ns

module mult4x4_tb();

reg  [3:0] a;
reg  [3:0] b;
wire [7:0] p;

mult4x4 mult4x4_inst(
	.a(a),
	.b(b),
	.p(p)
);

initial begin
	a = 1;
	b = 2;
	forever
		#2 a = a + 1;
end

endmodule

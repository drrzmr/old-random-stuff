`timescale 1 ns/1 ns

module adder_tb();

reg  [15:0] dataa;
reg  [15:0] datab;
wire [15:0] out;

adder adder_inst(
	.a(dataa),
	.b(datab),
	.sum(out)
);

initial begin
	dataa = 1;
	datab = 1;
end	

endmodule

module counter(
	input        CLOCK,
	input	       RESET,
	input        ENABLE,
	output [7:0] OUTPUT
);

reg [31:0] counter;

assign OUTPUT[7:0] = counter[31:24];

always @(posedge CLOCK, negedge RESET)
begin
	if (0 == RESET) begin
		counter <= 32'b0;
	end else if (ENABLE) begin
		counter <= counter + 1;
	end
end

endmodule

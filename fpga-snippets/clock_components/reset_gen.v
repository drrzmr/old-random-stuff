module reset_gen #(parameter CYCLES = 500000000)(
	input      CLOCK,
	input      IN,
	output reg OUT
);

reg [31:0]reset_cycles = 0;

always @(posedge CLOCK)
begin
	if (IN == 0) begin
		if (reset_cycles < CYCLES) begin
			reset_cycles <= reset_cycles + 1;
		end else begin
			reset_cycles <= 32'b0;
		end
	end
	
	if (IN == 1) begin
		reset_cycles <= 32'b0;
	end
end

always @(posedge CLOCK)
begin
	if (reset_cycles == CYCLES) begin
		OUT <= 0;
	end else begin
		OUT <= 1;
	end
end

endmodule

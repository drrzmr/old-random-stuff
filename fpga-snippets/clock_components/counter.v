module counter(
	input            CLOCK,
	input            ENABLE,
	input            RESET,
	output reg [31:0]OUT
);

always @(posedge CLOCK, negedge RESET)
begin
	if (0 == RESET) begin
		OUT <= 32'b0;
	end else begin
		if (1 == ENABLE) begin
			OUT <= OUT + 1;
		end
	end
end

endmodule

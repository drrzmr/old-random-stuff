# Introdução

## Acessando

Os scripts que fazem a instalação do ambiente configuram o webserver para atender
as requisições na porta 8080 assim como o vagrant faz forwarding dessa porta, então
basta acessar pelo browser na porta 8080 a raiz do site: `http://localhost:8080`, ou
utilizar o ip de onde está rodando a aplicação.

## Deploy
Para realizar o deploy automatizado por utilizar o fabric, o vagrant utiliza ele
no provisionamento. O fabfile implementa duas tasks `deploy` e `clear`, que são
auto explicativas.

# Descrição da Solução

## Árvore de diretórios

```
├── bbb
│   ├── api
│   │   └── resourses
│   ├── helpers
│   ├── site
│   │   ├── generators
│   │   ├── output
│   │   └── templates
│   └── vagrant
└── Logica_de_programacao
```

## bbb

Implementaçao da votaçao do Big Brother.

- **Vagrantfile** tem os parâmetros para provisionamento do vagrant
- **fabile.py** input para o fabric.

## bbb/api

Código python (Flask) que implementa a api rest.

- **\_\_init\_\_.py** tem a inicialização desses recursos

## bbb/api/resourses

Contem arquivos que tratam os requests para os resourses expostos pela rest

- **votes.py** /votes (GET/POST)
  - GET lista os votos por candidato até o momento, usado em result.html
  - POST forma de se votar, usado por wall.html
- **statistics.py** /statistics (GET)
  - GET estatísticas gerais da votação, usado por statistics.html
- **candidates.py** /candidates (GET)
  - GET lista os participantes da votação, usado por wall.html

## bbb/helpers

Coleção de shellscripts e templates de arquivos de configuração, utilizados para o setup do ambiende.

- **00.tunning-root-install.sh** tunning so ubuntu (número de conexões, número de filedescriptors).
- **01.env-root-install.sh** instala as ferramentas básicas para os outros scripts, atualmente apenas o pip3
- **10.mongo-root-install.sh** instala o mongodb (ubuntu e debian)
- **11-redis-root-install.sh** instala o redis (ubuntu e debian)
- **50.uwsgi-root-install.sh** instala e configura o uwsgi (ubuntu)
- **80.nginx-root-install.sh** instala e configura o nginx (ubuntu)
- **90.ab-root-install.sh** instala o apache2-utils para usar em teste de carga (ab)
- **01.env-user-setup.sh** instala os pacotes python no virtualenv usando pip
- **root-install.sh** executa todos os scripts com o nome `*-root-install.sh` na ordem de 00 para 99
- **user-install.sh** executa todos os scripts com o nome `*-user-*` na ordem de 00 para 99
- **root-config.sh** executa todos os scripts com o nome `*-root-condig.sh` na ordem de 00 para 99
- **clear.sh** script utilizado no test e desenvolvimento, limpa a vm
- **test-ab.sh** executa teste de carga usando o ApacheBenchmark (ab)
- **requirements.txt** contém os pacotes pip que a api e o site dependem
- **nginx.tpl.conf** template para a configuração do nginx
- **uwsgi.tpl.ini** template para a configuração do uwsgi

## bbb/site

Contém a implementação do frontend.

- **build.py** é responsável por inicializar o staticjinja (supporte para gerar arquivos estáticos usando jinja2).
- **Makefile** parâmetros para o make executar o build do site estático, use: make clean all para rebuild e teste.

## bbb/site/generators

Contém os geradores das variáveis utilizadas pelo staticjinga/jinja2

- **config.py** variáveis básicas de configuração
- **result.py** variáveis para result.html, inclui as variáveis de config.py
- **statistics.py** variáveis para statistics.html, inclui as variáveis de config.py
- **wall.py** variáveis para wall.html, inclui as variáveis de config.py

## bbb/site/output

Contém o resultado do build, é o root do site.

## bbb/site/templates

Contém os templates de input para o staticjinja/jinja2 e também arquivos estáticos que são apenas copiados para o output,
conforme setado no build.py

- **bootstrap-3.3.4-dist/** pacote com o bootstrap, apenas copiado para output
- **css/** contém o arquivo bbb.css com alguns poucos sets que foram nescessários, apenas copiado para output
- **img/** contém as imagens, apenas copiado
- **js/** bibliotecas de terceiro (jquery, raphael e morris), apenas copiado

- **\_nav.html** template jinja2, incluido apenas pelos outros templates
- **result.html**, wall.html e statistics.html templates para as páginas de mesmo nome
- **result.js**, statistics.js wall.js templates para o scripts incluidos pelos respectivos arquivos html

## bbb/vagrant

Contém o shellscript (`provision.sh`) utilizado no provisionamento da vm, que por sua vez executa o fabric de dentro
da vm.

## Logica\_de\_programacao

Resoluçao dos problemas de Lógica

- **Makefile** informa o make como realizar a compilação das duas soluções
- **collatz.c** resolução para a conjectura de collatz, aceita como parâmetro de linha de comando o número de elementos da sequência máxima
- **collatz.sh** exemplo de como executar o collatz
- **subcadeia.c** resoluçao para a subcadeira, o input é através de stdin
- **subcadeia.sh** exemplo de como executar o subcadeia, outra possível é: `echo "2 -4 6 8 -10 100 -6 5" | ./subcadeia`

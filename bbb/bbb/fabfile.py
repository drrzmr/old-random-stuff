from fabric.api import local
from fabric.api import run
from fabric.api import put
from fabric.api import sudo
from fabric.api import lcd
from fabric.api import reboot


def deploy():
    with lcd('../'):
        local('git archive --format tar.gz master bbb > /tmp/xbbb.tar.gz')
    run('rm -rf /tmp/bbb.tar.gz')
    put('/tmp/xbbb.tar.gz', 'bbb.tar.gz')
    run('rm -rf deploy')
    run('mkdir deploy')
    run('tar xf bbb.tar.gz -C deploy')
    sudo('./deploy/bbb/helpers/root-install.sh')
    run('./deploy/bbb/helpers/user-install.sh')
    sudo('./deploy/bbb/helpers/root-config.sh')


def clear():
    sudo('./deploy/bbb/helpers/clear.sh')

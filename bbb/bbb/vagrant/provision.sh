#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

apt-get -y install fabric git

su -c "cd /home/vagrant/repo/bbb && fab deploy -u vagrant -p vagrant -H 127.0.0.1" vagrant

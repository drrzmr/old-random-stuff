from flask.ext import restful
from flask import jsonify


class Candidates(restful.Resource):

    __candidates__ = [
        {
            'name': 'Vinícius de Moraes',
            'born': '1913-10-19',
            'image': 'vinicius.jpg',
            'id': '1'
        }, {
            'name': 'Clarice Lispector',
            'born': '1920-12-10',
            'image': 'clarice.jpg',
            'id': '2'
        }
    ]

    def get(self):
        return jsonify({'candidates': self.__candidates__})

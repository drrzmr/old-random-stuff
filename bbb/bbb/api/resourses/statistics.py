from flask.ext import restful
from flask.json import jsonify
from flask_restful import reqparse
from flask_restful import fields
from api import redis


def generate_tree(keys, values):
    t = {}
    i = 0
    for key in keys:
        key = key.decode('utf8')
        (year, month, day, hour) = key.split(':')[1:]

        if year not in t:
            t[year] = {}
        if month not in t[year]:
            t[year][month] = {}
        if day not in t[year][month]:
            t[year][month][day] = {}
        if hour not in t[year][month][day]:
            t[year][month][day][hour] = 0

        t[year][month][day][hour] = int(values[i].decode('utf-8'))
        i += 1
    return t


class Statistics(restful.Resource):

    def get(self):

        p = redis.pipeline()
        p.get('candidate:1')
        p.get('candidate:2')
        p.keys('votes_by_hour:*')
        (c1, c2, keys) = p.execute()
        values = [] if not keys else redis.mget(keys)
        intc1 = 0 if c1 is None else int(c1)
        intc2 = 0 if c2 is None else int(c2)

        total = intc1 + intc2
        candidates = [
            {'id': 1, 'name': 'Vinícius de Moraes', 'votes': intc1},
            {'id': 2, 'name': 'Clarice Linspector', 'votes': intc2}
        ]

        return jsonify(total=total, candidates=candidates,
                       tree=generate_tree(keys, values))

    def options(self):
        return ''

#!/bin/bash

# based on:
# https://rtcamp.com/tutorials/linux/increase-open-files-limit/
# http://tweaked.io/guide/kernel/

uid=$(id -u)
if [ ${uid} -ne 0 ]
then
    echo
    echo " -> you must be root!"
    echo
    exit -1
fi

# limits.conf
cat > /etc/security/limits.d/bbb.conf << EOF
*    hard nofile 500000
*    soft nofile 500000
root soft nofile 500000
root hard nofile 500000
EOF

# pam
PAM_LIMITS=$(grep session /etc/pam.d/common-session | grep required | grep pam_limits.so)
if [ -z "${PAM_LIMITS}" ]
then
	echo "session required pam_limits.so" >> /etc/pam.d/common-session
fi

# sysctl
cat > /etc/sysctl.d/99-bbb.conf << EOF
fs.file-max = 2097152
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.core.somaxconn = 10240
EOF

service procps start

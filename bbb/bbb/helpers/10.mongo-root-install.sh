#!/bin/bash

KEY="7F0CEB10"
KEY_SERVER="hkp://keyserver.ubuntu.com:80"
FINGER="492EAFE8CD016A07919F1D2B9ECBEC467F0CEB10"
PACKAGES="
mongodb-org=3.0.2
mongodb-org-server=3.0.2
mongodb-org-shell=3.0.2
mongodb-org-mongos=3.0.2
mongodb-org-tools=3.0.2
"

read_finger()
{
	echo $(apt-key finger | grep -A 1 $KEY | tail -n1 | cut -d"=" -f2 | tr -d " ")
}

# if mongo is installed do nothing
for pkg in ${PACKAGES}; do
	pkg_name=$(cut -d"=" -f1 <<< ${pkg})
    pkg_status=$(dpkg -l ${pkg_name} 2> /dev/null | tail -n1 | cut -d" " -f1)
    if [ "ii" != "${pkg_status}" -a "hi" != "${pkg_status}" ]; then
		pkg_list+="${pkg} "
    fi
done
if [ -z "${pkg_list}" ]; then
	echo
	echo " -> mongo already installed :)"
	echo
	exit 0	
fi

# setup sources.list
dist=$(lsb_release -si)
filename="/etc/apt/sources.list.d/mongodb-org-3.0.list"
case ${dist} in
	"Debian")
		echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.0 main" | tee ${filename}
		;;
	"Ubuntu")
		echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | tee ${filename}
		;;
	*)
		echo
		echo "* oops, I don't know how install mongo in your system"
		echo
		exit -1
esac

# install key to apt
finger=$(read_finger)
if [ -z $finger ]; then
	apt-key adv --keyserver ${KEY_SERVER} --recv ${KEY}
fi

# validate fingerprints 
finger=$(read_finger)
if [ "${FINGER}" != "${finger}" ]; then
	echo
	echo "* oops, crazy fingerprints"
	echo
	exit -2
fi

apt-get -q update
apt-get -q -y install ${pkg_list}
status=${?}
if [ ${status} -ne 0 ]; then
	echo
	echo " -> mongo is installed now \\o/"
	echo
	exit ${status}
fi

# hold mongo packages
for i in ${PACKAGES}; do
	pkg_name=$(cut -d"=" -f1 <<< ${i})
	dpkg --set-selections <<< "${pkg_name} hold"
done

echo
echo " -> mongo is installed now \\o/"
echo

#!/bin/bash

REQUESTS="10000"
CONCURRENCY="5000"
HOST="127.0.0.1"
PORT="8080"
CONTENT_TYPE="application/json"
RESOURCE="votes"
LOG="/tmp/ab.log"
JSON="/tmp/ab.post.json"
cat > ${JSON} << EOF
{
  "id": "1",
  "name":"Vinícius de Moraes"
}
EOF
URL="http://${HOST}:${PORT}/${RESOURCE}"

echo REQUEST: ${REQUESTS}
echo CONCURRENCY: ${CONCURRENCY}
echo URL: ${URL}
echo CONTENT_TYPE: ${CONTENT_TYPE}
echo JSON:
cat ${JSON}

cmd="ab -v 10 -T ${CONTENT_TYPE} -p ${JSON} -n ${REQUESTS} -c ${CONCURRENCY} ${URL}"

echo
echo "-> ${cmd}"
echo
for i in 3 2 1; do
	echo -n ${i}...
	sleep 1
done
echo

ab -v 10 -T ${CONTENT_TYPE} -p ${JSON} -n ${REQUESTS} -c ${CONCURRENCY} ${URL} > ${LOG}

tail -n 38 ${LOG}
echo
grep "^LOG: Response" ${LOG} | sort | uniq
rm -f ${LOG} ${JSON}

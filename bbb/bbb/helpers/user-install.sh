#!/bin/bash

uid=$(id -u)
if [ ${uid} -eq 0 ]
then
	echo
	echo " -> you must not be root!"
	echo
	exit -1
fi

echo
echo " -> I'm $(basename $0)"
echo

SCRIPT_DIR=$(cd $(dirname $0) && pwd)
SITE_DIR=$(cd $(dirname $0)/../site && pwd)

cd ${SCRIPT_DIR}
for script in *-user-*.sh; do
	echo
	echo "-> running ${script}"
	echo
	./${script}
done

make -C ${SITE_DIR}

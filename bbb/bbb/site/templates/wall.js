$(document).ready(function()
{
  cid = 0;
  API_URL = "{{API_URL}}";
  i_am_human = false;

  $('.candidate a').on('click', function(e) {
    $('.candidate').find('a').removeClass('selected');
    $('.candidate').find('a').addClass('unselected');
    $(this).removeClass('unselected');
    $(this).addClass('selected');
  });

  var url_candidates = API_URL + '/candidates';
  $.getJSON(url_candidates, function(data)
  {
    c1a = $('#c1 a');
    c2a = $('#c2 a');

    c1 = data['candidates'][0];
    c1a.attr('candidate_id', c1.id);
    c1a.find('h5').text(c1.name);
    c1a.find('img').attr('src', '{{IMG_DIR}}/' + c1.image);

    c2 = data['candidates'][1];
    c2a.attr('candidate_id', c2.id);
    c2a.find('h5').text(c2.name);
    c2a.find('img').attr('src', '{{IMG_DIR}}/' + c2.image);
  }).fail(function(jq, st, e) {
    alert('Problemas em contatar o servidor: ' + url_candidates);
  });

  $('#voteButton').click(function(e) {

    cid = $('.candidate').find('.selected').attr('candidate_id');

    if (0 == cid || undefined == cid) {
      $('.alert').addClass('hidden');
      $('.alert-danger').removeClass('hidden');
      $('.alert-danger').addClass('xsel');
      $('#alertModal').modal('show');
      return;
    }

    if (true == i_am_human) {
      do_vote(grecaptcha.getResponse());
      return;
    }

    $('#myModal').modal('show');
  });

  $('#alertModal').on('hide.bs.modal', function(e) {
    var xsel = $('.xsel');
    xsel.removeClass('xsel');
    var dst = xsel.attr('dst');
    if ('wall.html' != dst)
      $(location).attr('href', dst);
  });
});

function do_vote(response)
{
  $('#myModal').modal('hide');
  $('#waitVoteModal').modal('show');

  $.ajax({
    type: "POST",
    url: API_URL + '/votes',
    data: JSON.stringify({
      id: cid,
      grecaptcha: response,
    }),
    dataType: 'json',
    traditional: false,
    accepts: 'application/json',
    contentType: 'application/json',
    success: function(data)
    {
      $('.alert').addClass('hidden');
      $('.alert-success').removeClass('hidden');
      $('.alert-success').addClass('xsel');

      $('#waitVoteModal').modal('hide');
      $('#alertModal').modal('show');
    },
    error: function(jq, st, e)
    {
      $('#waitVoteModal').modal('hide');
      alert(jq.responseJSON.message);
    }
  });
}

function recaptchaCallback(response)
{
  i_am_human = true;
  do_vote(response);
}

function recaptchaExpiredCallback()
{
  i_am_human = false;
}

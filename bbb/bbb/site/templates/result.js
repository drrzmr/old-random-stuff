$(document).ready(function()
{
  donut = Morris.Donut({
    element: 'donut',
    data: [
      {label: 'A', value: 10},
      {label: 'B', value: 10}
    ]
  });

  var url_votes = "{{API_URL}}" + '/votes';
  $.getJSON(url_votes, function(data)
  {
    console.log(data);
    var votes = data['votes'];

    donut.setData([
      {label: votes[0].name, value: votes[0].votes},
      {label: votes[1].name, value: votes[1].votes}
    ]);
  }).fail(function(jq, st, e) {
    alert('Problemas em contatar o servidor: ' + url_votes);
  });

  $('#vote').on('click', function() {
    $(location).attr('href', 'wall.html');
  });
});

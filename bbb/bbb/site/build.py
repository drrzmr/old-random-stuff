import staticjinja as sj

OUTPUT_DIR = 'output'
TEMPLATES_DIR = 'templates'
STATIC_DIRS = ['css', 'img', 'js', 'bootstrap-3.3.4-dist']

from generators import statistics
from generators import wall
from generators import result
from generators import config

contexts = [
    ('statistics.html', statistics.gen),
    ('wall.html', wall.gen),
    ('result.html', result.gen),
    ('index.html', config.gen),
    ('result.js', config.gen),
    ('wall.js', config.gen),
    ('statistics.js', config.gen)
]

if __name__ == '__main__':
    site = sj.make_site(contexts=contexts, outpath=OUTPUT_DIR,
                        searchpath=TEMPLATES_DIR, staticpaths=STATIC_DIRS)
    site.render()

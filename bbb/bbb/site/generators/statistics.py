from generators import config


def gen():
    ret = {
        'statistics_js': 'statistics.js'
    }
    ret.update(config.gen())

    return ret

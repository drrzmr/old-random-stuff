package command

import (
	"github.com/drrzmr/cafica/config"
	"github.com/drrzmr/cafica/logger"
)

// Context object
type Context struct {
	conf    config.Config
	command string
	log     logger.Logger
	flags   map[string]bool
}

func newContext(conf config.Config, command string, log logger.Logger, flags map[string]bool) *Context {
	return &Context{
		conf:    conf,
		command: command,
		log:     log,
		flags:   flags,
	}
}

// Config retrieve config object
func (context *Context) Config() config.Config {
	return context.conf
}

// Command retrieve command string representation
func (context *Context) Command() string {
	return context.command
}

// Log retrieve log object
func (context *Context) Log() logger.Logger {
	return context.log
}

// Flag retrieve a flag value
func (context *Context) Flag(name string) bool {
	return context.flags[name]
}

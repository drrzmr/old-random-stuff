package data

// Table with keys (topic name) and rows (topic information)
type Table map[string]Rows

// Append a row to table
func (table Table) Append(row Row) {

	if rows, ok := table[row.Topic]; ok {
		table[row.Topic] = append(rows, row)
	} else {
		table[row.Topic] = Rows{row}
	}
}

package cafica

import (
	"sync"

	"github.com/Shopify/sarama"
	"github.com/pkg/errors"
)

// SaramaConnector implements Connector interface using sarama library
type SaramaConnector struct {
	Connector

	nextID       uint32
	idMutex      sync.Mutex
	caficaConfig Config
	saramaConfig *sarama.Config
}

// NewSaramaConnector create Connector using sarama library
func NewSaramaConnector(config Config) (Connector, error) {

	saramaVersion, err := sarama.ParseKafkaVersion(config.Version)
	if err != nil {
		return nil, errors.Wrap(err, "invalid kafka version")
	}

	saramaConfig := sarama.NewConfig()
	saramaConfig.ClientID = config.ID
	saramaConfig.Version = saramaVersion

	return &SaramaConnector{
		saramaConfig: saramaConfig,
		caficaConfig: config,
		nextID:       uint32(0),
		idMutex:      sync.Mutex{},
	}, nil
}

func (sc *SaramaConnector) genClientID() uint32 {

	sc.idMutex.Lock()
	defer sc.idMutex.Unlock()
	id := sc.nextID
	sc.nextID++

	return id
}

// Connect creates a connection to broker
func (sc *SaramaConnector) Connect() (Client, error) {

	backend, err := sarama.NewClient(sc.caficaConfig.Brokers, sc.saramaConfig)
	if err != nil {
		return nil, errors.Wrap(err, "could not create sarama connectors")
	}

	return &SaramaClient{
		ID:      sc.genClientID(),
		backend: backend,
	}, nil
}

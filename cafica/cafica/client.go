package cafica

// Client interface
type Client interface {
	Connection

	Close() error
}

// Connection interface
type Connection interface {
	Topics() ([]string, error)
	Partitions(topic string) (string, []int32, error)
	Offsets(topic string, partition int32) (string, int32, int64, int64, error)
}

package config

import (
	"github.com/drrzmr/cafica/cafica"
	"github.com/drrzmr/cafica/logger"
)

// Config object for cafica cli line tool
type Config struct {
	Kafka    cafica.Config
	LogLevel logger.LogLevel
}

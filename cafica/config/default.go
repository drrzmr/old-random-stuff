package config

import (
	"github.com/drrzmr/cafica/cafica"
	"github.com/drrzmr/cafica/logger"
)

// Default returns default configuration
func Default() Config {
	return Config{
		LogLevel: logger.WarnLevel,
		Kafka: cafica.Config{
			ID:      "cafica",
			Version: "0.10.1.1",
			Backend: cafica.SARAMA,
			Brokers: []string{
				"localhost:9092",
			},
		},
	}
}
